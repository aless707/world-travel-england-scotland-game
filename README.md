#About

"World Travel: England & Scotland" is a 2D educational point-and-click walking simulator graphics game for Windows PC.
* Target audience - children aged 8 to 12
* Language - English
* Controls - kayboard and mouse
* Task - read the given educational information and answer the questions

Created by a final year Vytautas Magnus university (VMU) student:

* Sandra Pučkoriūtė - idea, design, art, animation, story, programming

*Used photos, music, sounds, textures, patterns and fonts are credited in game.*


## Screenshots

![Title screen](https://i.imgur.com/WaJShtL.png)
![Pause menu](https://i.imgur.com/HB7qKf0.jpg)

![Wrong answer](https://i.imgur.com/8Ap7KDM.png)
![Right answer](https://i.imgur.com/bX1FIUl.png)

![Prize](https://i.imgur.com/vcYdBcd.jpg)
![Educational information](https://i.imgur.com/p6iA3XC.jpg)

## Tools

* Unity (C#) - game creation, programming

* Paint Tool SAI, Adobe Photoshop CC 2018 - art

* Anima 2D - animation